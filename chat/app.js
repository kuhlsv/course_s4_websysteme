#!/usr/bin/node
const settings = require('./settings.json'),
  MongoClient = require('mongodb').MongoClient,
  app = require('express')(),
  bodyParser = require('body-parser'),
  expressWs = require('express-uws')(app),
  bearerToken = require('express-bearer-token'),
  request = require('request-promise-native'),
  WebsocketHandler = require('./websockets/websocketHandler'),
  init_chat = require('./websockets/websocketChat'),
  authUtils = require('./helpers/authUtils'),
  init_persistance = require('./helpers/chatPersitanceHandler'),
  history_resource = require('./resources/history'),
  known_chats_resource = require('./resources/lastChats'),
  ack_message_resource = require('./resources/ackMessage'),
  new_messages_resource = require('./resources/newMessages');

// Enabling Bearer Authorization
app.use(bearerToken());

// Addding JSON body parser for JSON consumption 
app.use(bodyParser.json());

// Disabling caching
app.disable('etag');

// Allowing CORS, settings additional Cache-Control header to make sure responses won't cached by clients
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.header('Cache-Control', 'no-cache');
  if (!req.token) {
    next();
    return;
  } else {
    authUtils.whoami(req.token)
      .then((user) => {
        req.user = user;
      })
      .then(() => next())
      .catch((e) => {
        console.log("Catch", req.token);
        next();
      });
  }
});

app.ws('/', (ws, req) => {
  // Just for registration of the websocket resource
});

// As we can't do anything without a MongoDB connection, we'll do the registration of all 
// needed resources within then().
MongoClient.connect(settings.mongodb.uri, { useNewUrlParser: true }).then((mongoClient) => {

  // Handles all incoming messages
  const handler = new WebsocketHandler(expressWs.getWss());
  
  // Initializes the chat
  init_chat(handler, mongoClient);

  // Initializes persistance of chats in MongoDB
  init_persistance(handler, mongoClient);

  app.get('/history/:partner', history_resource(mongoClient));
  app.get('/known-chats', known_chats_resource(mongoClient));
  app.post('/ack/:id', ack_message_resource(mongoClient));
  app.get('/new-messages', new_messages_resource(mongoClient));
});

app.listen(process.env.WS_PORT || settings.server.port, settings.server.host);