const settings = require('../settings.json'),
  ObjectId = require('mongodb').ObjectID

module.exports = (mongoClient) => {
  return {
    ack: function(id, username) {
      return new Promise((resolve, reject) => {
        mongoClient.db(settings.mongodb.database)
          .collection(settings.mongodb.collection)
          .update({
            "_id":  id
          }, {$addToSet: { ackBy: username } }, (err, result) => {
            if (err) {
              console.error(err);
              reject(err);
              return;
            }
            resolve();
          });
      });
    },
    countNew: function(username) {
      return new Promise((resolve, reject) => {
        mongoClient.db(settings.mongodb.database)
        .collection(settings.mongodb.collection)
        .find({ to: username, ackBy: { $nin: [username]} })
        .count((err, resp) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(resp);
        });
      });
    }
  }
}
