const settings = require('../settings.json'),
 request = require('request-promise-native');

// Will cache the user. The map will point from token to user.
let userCache = {};

/**
 * This module provides methods that are important for authentication and authorization.
 * 
 * @author cfender
 */
module.exports = {
  /**
   * Fetches the user based on the passed token. It caches all users for a minute.
   */
  whoami: (token) => {
    if (userCache[token]) {
      return new Promise((resolve, reject) => {
        resolve(userCache[token]);
      });
    } else {
      return request.get(settings.whoami_endpoint, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }).then((resp) => {
        let user = JSON.parse(resp);
        userCache[token] = user;
        // Removing user after timeout to prevent memory leaks
        setTimeout(() => {
          delete userCache[token];
        }, 60 * 1000);
        return user;
      });
    }
  }
}