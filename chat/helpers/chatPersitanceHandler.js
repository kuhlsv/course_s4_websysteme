const settings = require('../settings.json');

/**
 * Module for persitance of private messages.
 * 
 * @param {*} handler 
 * @param {*} mongo
 * @author cfender 
 */
module.exports = (handler, mongo) => {
  handler.on('prv_msg', (ws, payload) => {
    let from = ws.user.username;
    let doc = Object.assign({}, payload, { from, date: new Date() });
    mongo.db(settings.mongodb.database).collection(settings.mongodb.collection).insert(doc);
  });
}