const settings = require('../settings'),
  authUtils = require('../helpers/authUtils')
  AckMessageHelper = require('../helpers/ackMessageHelper'),
  ObjectId = require('mongodb').ObjectID;

module.exports = (mongoClient) => {
  return (req, res) => {
    // Don't do anything if the user is not logged in or no partner username was passed
    if (!req.user) {
      res.sendStatus(401);
      return;
    }
    let ackMessageHelper = AckMessageHelper(mongoClient);
    let messageId = new ObjectId(req.params.id);
    ackMessageHelper.ack(messageId, req.user.username).then(() => {
      res.sendStatus(200);
    }).catch((e) => {
      console.error(e);
      res.sendStatus(500);
    });
  }
};