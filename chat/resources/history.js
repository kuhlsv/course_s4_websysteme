const settings = require('../settings'),
  authUtils = require('../helpers/authUtils');

// Default limit for fetching documents from MongoDB
const DEFAULT_LIMIT = 10;

module.exports = (mongoClient) => {
  const ackMessageHelper = AckMessageHelper(mongoClient);
  return (req, res) => {
    let partner = req.params.partner;
    // Don't do anything if the user is not logged in or no partner username was passed
    if (!partner || !req.user) {
      res.sendStatus(!partner ? 400 : 401);
      return;
    }
    // Get the current user
    let to = req.user.username;
    let users = { $in: [to, partner] };
    let query = {
      // As we need to fetch all entries matching { from: "A", to: "B" } and { from: "B", to: "A" } we combine those queries
      $or: [{
        from: partner,
        to
      }, {
        from: to,
        to: partner
      }]
    };
    // For loading additional messages
    if (req.query.before) {
      query.date = { $lt: new Date(req.query.before) };
    }
    mongoClient.db(settings.mongodb.database)
                .collection(settings.mongodb.collection)
                .find(query)
                .sort({ date: -1 })
                .limit(req.query.limit ? parseInt(req.query.limit) : DEFAULT_LIMIT)
                .toArray((err, documents) => {
      if (err) {
        res.sendStatus(500);
        res.send();
      } else {
        res.setHeader('Content-Type', 'application/json');
        let msgs = documents.map((doc) => {
          // Adding additional isSelf field which will be set to true if the message is coming
          // from the requesting user
          doc.isSelf = doc.from !== partner;
          return doc;
        });
        // Filter messages which have not been acknowledget yet and acknowledge them
        msgs.filter((doc) => !doc.ackBy || doc.ackBy.indexOf(req.user.username) !== -1).forEach((msg) => {
          ackMessageHelper.ack(msg._id, req.user.username);
        });
        res.send(JSON.stringify(msgs));
      }
    });
  }
};