const settings = require('../settings'),
  authUtils = require('../helpers/authUtils');

module.exports = (mongoClient) => {
  return (req, res) => {
    // Don't do anything if the user is not logged in or no partner username was passed
    if (!req.user) {
      res.sendStatus(401);
      return;
    }
    // Queries all messages whose recipient or sender corresponds to the current user
    let query = [
      { $match: { $or: [{ to: req.user.username }, { from: req.user.username }] }},
      { $sort: { date: -1 } },
      { $group: { _id: null, uniqueValues: { $addToSet: { from: '$from', to: '$to' } } } }
    ];
    mongoClient.db(settings.mongodb.database)
          .collection(settings.mongodb.collection)
          .aggregate(query)
          .toArray((err, doc) => {
      if (err) {
        res.sendStatus(500);
        return;
      }
      res.setHeader('Content-Type', 'application/json');

      if (!doc[0] || !doc[0].uniqueValues) {
        res.send("[]");
        return;
      }
      
      // Generating a flat array with all chat partners
      let chats = doc[0].uniqueValues.map((value) => {
        if (value.to === req.user.username) {
          return value.from;
        }
        return value.to;
      }).filter((value, index, self) => {
        return self.indexOf(value) === index;
      // Value should never be null or undefined, but just to make sure
      }).filter(value => value !== null && value !== undefined && value !== req.user.username);
      res.send(JSON.stringify(chats));
    });
  }
};