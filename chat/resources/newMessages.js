const settings = require('../settings'),
authUtils = require('../helpers/authUtils')
AckMessageHelper = require('../helpers/ackMessageHelper');

module.exports = (mongoClient) => {
  return (req, res) => {
    if (!req.user) {
      res.sendStatus(401);
      return;
    }
    let ackMessageHelper = AckMessageHelper(mongoClient);
    ackMessageHelper.countNew(req.user.username).then((count) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({ count }));
    }).catch((e) => {
      console.error(e);
      res.sendStatus(500);
    });
  }
};