const UUID = require('uuid/v1'),
  settings = require('./../settings.json'),
  authUtils = require('../helpers/authUtils'),
  request = require('request-promise-native'),
  AckMessageHelper = require('../helpers/ackMessageHelper');

/**
 * Module handles chat events emitted by the WebsocketHandler. It handles authorization requests, private messages, etc.
 * 
 * @param {WebsocketHandler} handler
 * @author cfender
 */
module.exports = (handler, mongoClient) => {
  /**
   * Handles authorization/authentication of users. As soon as an client etablishes a connection it must send
   * the JWT token. It will be forwarded to the main service which will return the user data. The user data will
   * be assigned to the respective websocket instance.
   */
  handler.on('auth', (ws, payload) => {
    console.log('Got authentication request, token was: ', payload.token);
    authUtils.whoami(payload.token).then((user) => {
     
      // For security reasons we'll close the socket whenever an invalid token was receiven
      if (!user) {
        ws.close();
      }

      // Appending the user to the websocket instance for later usage
      ws.user = user;

      // Caching all known websocket instances of each user
      if (!handler.userToSockets[user.username]) {
        handler.userToSockets[user.username] = [];
      }
      handler.userToSockets[user.username].push(ws);

      // Sending acknowledge message
      ws.send(JSON.stringify({
        action: 'AUTH_ACK'
      }));
    }).catch((err) => {
      console.error('An error occurred: ', err);
      // Notifiying about failed auth request
      ws.send(JSON.stringify({
        action: 'AUTH_FAILED'
      }));
    });
  });

  handler.on('msg_ack', (ws, data) => {
    let ackMessageHelper = AckMessageHelper(mongoClient);
    ackMessageHelper.ack(data.id, ws.user.username);
  });

  handler.on('prv_msg', (ws, data) => {
    // Won't do anything if not authenticated
    if (!ws.user) return;
    JSON.stringify({
      action: 'PRV_MSG_ACK',
      msg: {
        id: data.msg.id
      }
    });
    let unique = [data.to, ws.user.username].filter((value, index, self) => self.indexOf(value) === index);
    console.log(`Forwarding to ${unique}. Known users: ${Object.keys(handler.userToSockets)}`);
    unique.forEach((recipients, a) => {
      try {
        // If there is no known websocket for the user (this is always the case if the user is not currently online)
        // we won't go on.
        if (!handler.userToSockets[recipients]) return;
        handler.userToSockets[recipients].forEach((wsUser) => {
          wsUser.send(JSON.stringify({
            action: 'PRV_MSG',
            msg: {
              from: ws.user.username,
              msg: data.msg,
              id: UUID(), // This id will be only used provisionally. Later we'll replace it with the ObjectId stored in MongoDB
              isSelf: recipients === ws.user.username,
              date: new Date()
            }
          }));
        });
      } catch (e) {
        console.error(`An unexpected error occured: ${e}`);
      }
    });
  });
};