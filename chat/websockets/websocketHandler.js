const EventEmmitter = require('events'),
 ObjectId = require('mongodb').ObjectID;

/**
 * Simple handler for websocket events. It inherits from EventEmmitter. Whenever an incoming message is being receiven
 * it will emit an event, the name of it will be identified by the action field of the payload.
 * 
 * @author cfender
 */
class WebsocketHandler extends EventEmmitter {
  
    constructor(wss) {      
      super();

      /**
       * Preserves all websocket instances mapped to the respective username (key: username, value: wsInstance)
       */
      this.userToSockets = {};
      
      wss.on('connection', (ws) => {

        ws.on('message', (data) => {
          let payload = JSON.parse(data);
          // Prevent resumption if we receive a malformed message
          if (!payload.action) return;
          try {
            if (payload.action.toLowerCase() === 'prv_msg') {
              payload.data['_id'] = new ObjectId();
              console.log("Set id to", payload.data._id);
            }
            this.emit(payload.action.toLowerCase(), ws, payload.data);
          } catch (e) {
            console.error(`An error occured while parsing incoming message: ${e}`);
          }
        });

        // Remove user after closing websocket to avoid memory leaks
        ws.on('close', () => {
          if (ws.user && ws.user.username) {
            this.userToSockets[ws.user.username].filter((wsUser) => wsUser !== ws);
            // When there are no connections left, we'll remvoe the entry
            if (this.userToSockets[ws.user.username].length === 0) {
              delete this.userToSockets[ws.user.username];
            }
          }
        });
      });
    }
  }

  module.exports = WebsocketHandler;