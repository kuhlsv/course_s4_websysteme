import bcrypt
import pymongo
from flask import Blueprint, Response, jsonify, request
from flask_jwt import current_identity, jwt_required, current_identity

from db.models.registration import Registration
from db.models.passwordChangeRequest import PasswordChangeRequest
from db.models.user import User

from utils.sendmail import send

from string import Template

app = Blueprint('auth', import_name='auth', url_prefix='/auth')

@app.route('/reset-password', methods=['POST'])
@jwt_required()
def update_password():
  '''
  Updates the user password.
  '''
  # The change Request consists of the oldPassword, password and passwordConfirmation
  pw_change_request = PasswordChangeRequest.from_json(request.stream.read().decode('UTF-8'))

  # Verifying if the password equals the passwordConfirmation. It not, we'll return 400 (bad request)
  if pw_change_request.password != pw_change_request.passwordConfirmation:
    return Response(status=400)
  
  # Fetch the user from the mongodb
  user = User.objects.get(username=current_identity.username)
  
  # Check if the old password matches the passed one in the request
  if bcrypt.hashpw(pw_change_request.oldPassword, user.password) == False:
    # Return forbidden is it is not correct
    return Response(status=403)
  
  # Generate the hash + salt and pass it to the user model
  user.password = generate_hash(pw_change_request.password)
  user.save()

  # And we're done :)
  return Response(status=200)

@app.route('/local/register', methods=['POST'])
def register():
    '''
    Method handles user registration. After registration we'll send a confirmation email with an activation link
    ''' 
    try:
      # Registration contains all information we need to know (such as username, mail, etc.)
      registration = Registration.from_json(request.stream.read().decode('UTF-8'))

      # We'll save the user. The username and email is set to unique, so we'll get an exception if we create a duplicate
      user = User(username=registration.username, password=generate_hash(registration.password), email=registration.email).save()

      # Before we return the user object, we'll remove the password. It is just a hash but it should not leak anyway
      user.password = None

      # Template for plaintext mail
      plainTpl = Template("""
           Hallo $username,

           vielen Dank für deine Anmeldung.
           
           Für eine vollständige Aktivierung deines Accounts muss deine E-Mail Adresse bestätigt werden. Klicke hierzu auf den folgenden Link:\n
           
           $link

           Vielen Dank und Gruß\n
           Dein Gandalph Team!""")
      
      # Template for html mail
      htmlTpl = Template("""
           Hallo $username,<br>
           <br>
           vielen Dank für deine Anmeldung.<br>
           <br>
           Für eine vollständige Aktivierung deines Accounts muss deine E-Mail Adresse bestätigt werden. Klicke hierzu auf den folgenden Link:<br>
           <br>
           <a href="$link">$link</a><br>
           <br>
           Vielen Dank und Gruß<br>
           Dein Gandalph Team!""")

      # Sending Mail
      send(to=user.email, 
           subject="Aktivierung Gandalph Account", 
           plaintext=plainTpl.safe_substitute(username=user.username, link=request.url_root + "user/activate/" + str(user.id)), 
           html=htmlTpl.safe_substitute(username=user.username, link=request.url_root + "user/activate/" + str(user.id)))
      # HTTP ok if everything worked
      return Response(user.to_json(), status=200)
    # This should only happen, if there is already an user with this name/email
    except pymongo.errors.DuplicateKeyError as e:
      # Response is copied from strapi to match the REST API
      return Response('{"statusCode":400,"error":"Bad Request","message":"This username is already taken"}', status=400)

def generate_hash(pw):
  '''
  Method generates a password hash with the help of bcrypt module. It also generates a salt to make sure, we won't create the 
  same hash for the same password if this method is executed multiple times.
  '''
  salt = bcrypt.gensalt()
  return bcrypt.hashpw(pw, salt)
