"""
Provides the API for our newsfeed
"""
from api.default import register_default_resource
from db.models.channel import Channel
from db.models.post import Post
from flask import Response
from flask_jwt import jwt_required, current_identity
from db.models.user import User

# Generating a general blueprint for channels. It will be available under /channels
blueprint = register_default_resource(clz=Channel, endpoint='/channels', entity='channels') 

# Adding custom resources
@blueprint.route('<id>/posts', methods=['GET'])
def get_posts(id):
  '''
  Method fetches all posts from MongoDB and returns them.
  '''
  entity = Post.objects(channelId=id)
  return Response(entity.to_json(), status=200)

@blueprint.route('<id>/count', methods=['GET'])
def count(id):
  '''
  Returns the count for the posts in a channel
  '''
  entity = Post.objects(channelId=id).count()
  return str(entity)

@blueprint.route('<id>/subscribe', methods=['POST'])
@jwt_required()
def subscribe(id):
  ''''
  Allows subscribptions of channels
  '''
  entity = User.objects(id=current_identity.id).count()
  entity.channels.insert(id)
  entity.save()
  # Returning 201 as we don't return any content here
  return Response(status=201)