# This module contains general methods which are needed for all entity types.
# Whenever possible this should be used and extended if neccessary.
  
from flask import Blueprint, Response, request
from json import dumps
from flask_jwt import jwt_required

def register_default_resource(clz=None, endpoint='/', entity=None):
  '''
  Method generates a blueprint with the following methods: GET, POST, GET /<id>, DELETE
  
  :params clz: The Mongoengine class type
  :params endpoint: The endpoint where the API should be accessed
  :params entity: The entity name 
  '''
  app = Blueprint(str(clz), import_name=__name__, url_prefix=endpoint)
  @app.route('', methods=['GET'])
  @jwt_required()
  def get_entity():
    '''
    Returns a list of entities from mongodb. Allows pass limit and start (for skipping entries)
    '''
    limit = request.args.get('_limit', 30)
    skip = request.args.get('start', 0)
    entities = clz.objects().skip(int(skip)).limit(int(limit))
    return Response(response=entities.to_json())
  
  @app.route('', methods=['POST'])
  @jwt_required()
  def add_entities():
    '''
    Saves the passed entity or updates the existing one
    '''
    entity = clz.from_json(request.stream.read().decode('UTF-8')).save()
    return Response(entity.to_json(), status=200)
  
  @app.route('<id>', methods=['GET'])
  @jwt_required()
  def get_entity_by_id(id):
    '''
    Returns a single entity from mongodb.
    '''
    entity = clz.objects.get(id=id)
    return Response(entity.to_json(), status=200)
  
  @app.route('<id>', methods=['DELETE'])
  @jwt_required()
  def delete_entity(id):
    '''
    Deletes the entity with the given id.
    '''
    clz.objects(id=id).delete()
    return Response(status=200)
  
  return app