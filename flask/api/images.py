from db.models.images import Image
from db.models.upload_file import UploadFile

from flask import Blueprint, Response, request
from json import dumps

def init(current_app):
  '''
  Generates the blueprint for fetching images
  '''
  app = Blueprint('images', import_name='images', url_prefix='/images')

  @app.route('', methods=['GET'])
  def get_entity():
    '''
    We pass the name as search parameter and reutrn the image (or 404 if not found)
    '''
    image = Image.objects().get(name=request.args.get('search'))

    for file in UploadFile.objects():
      for ref in file.related:
        if ref['ref'] == image.id:
          return Response("{ \"image\": { \"url\": \"" + file.url + "\"} }", status=200)
    return Response(status=404)

  return app