"""
Provides the API for our newsfeed
"""
from db.models.news import News
from db.models.upload_file import UploadFile

from flask import Blueprint, Response, request
from json import dumps
from flask_jwt import jwt_required, current_identity

from utils.jwt import check_role

def init(current_app):
  '''
  Generates the blueprint for handling news entries
  '''
  app = Blueprint('news', import_name='news', url_prefix='/news')

  @app.route('', methods=['GET'])
  def get_entity():
    '''
    Fetches multiple news. Allows passing limit/start parameters.
    '''
    limit = request.args.get('_limit', 30)
    skip = request.args.get('start', 0)
    entities = News.objects().skip(int(skip)).limit(int(limit))
    return Response(response=entities.to_json())

  @app.route('', methods=['POST'])
  @jwt_required()
  def add_entities():
    '''
    Saves/updates an (existing) entry
    '''
    check_role(current_identity, "Administrator")
    entity = News.from_json(request.stream.read().decode('UTF-8')).save()
    return Response(entity.to_json(), status=200)

  @app.route('<id>', methods=['GET'])
  def get_entity_by_id(id):
    '''
    Fetches and returns the news with the given id.
    '''
    entity = News.objects.get(id=id)
    return Response(entity.to_json(), status=200)
  
  @app.route('<id>', methods=['DELETE'])
  @jwt_required()
  def delete_entity(id):
    '''
    Removes an news entry. Only allowed for administrators.
    '''
    check_role(current_identity, "Administrator")
    News.objects(id=id).delete()
    return Response(status=200)

  @app.route('/location', methods=['GET'])
  def news_by_location():
    '''
    Fetches all news for a given location.
    '''
    limit = request.args.get('_limit', 5)
    skip = request.args.get('start', 0)
    entities = News.objects(location=request.args.get('name')).skip(int(skip)).limit(int(limit))
    return Response(response=entities.to_json())

  return app