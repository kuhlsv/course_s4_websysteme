"""
Provides the API for our posts. It uses our register_default_resource for generating a generic resource
"""
from api.default import register_default_resource

from db.models.post import Post

blueprint = register_default_resource(clz=Post, endpoint='/posts', entity='posts')