'''
Module generates a blueprint for handling subscriptions of channels.
'''
from db.models.subscriptions import Subscription
from flask_jwt import jwt_required, current_identity
from flask import Blueprint, Response, request
from json import loads

app = Blueprint('subscriptions', import_name='subscriptions', url_prefix='/subscriptions')

@app.route('/<id>', methods=['GET'])
@jwt_required()
def get_subscriptions(id):
  '''
  Fetches the subscriptions of a user and returns them
  '''
  try:
    return Response(Subscription.objects.get(userId=id).to_json())
  except:
    return Response(status=404)

@app.route('', methods=['POST'])
@jwt_required()
def create_subscription():
  '''
  Method creates a subscription. For this we'll fetch the channelId from the passed JSON and append it to an existing
  subscription object (or we create a new one). It uses the user id detected by the current identity (which is injected
  by the flask_jwt library)
  '''
  # Loading the JSON to a simple dict, as we only need to get one value from it
  req = loads(request.stream.read().decode('UTF-8'))

  # The id of the currently logged in user
  userId = current_identity.id
  
  # Now we try to fetch the subscriptions from the mongodb. If it does not exist, we set subscription to none
  try:
    subscription = Subscription.objects.get(userId=userId)
  except Exception:
    subscription = None

  # If we have not found any subscription we're going to create a new Subscription instance
  if subscription is None:
    subscription = Subscription(userId=userId)

  # Add the requested channel
  subscription.channelIds.append(req['channelId'])

  # To make sure, we have only unique entries here, we create a sat based on the passed channelIds and convert it back
  # to a list again
  subscription.channelIds = list(set(subscription.channelIds))
  subscription.save()
  return Response()

@app.route('/<channelId>', methods=['DELETE'])
@jwt_required()
def delete_subscription(channelId):
  req = loads(request.stream.read().decode('UTF-8'))
  userId = current_identity.id

  sub = Subscription.objects.get(userId=userId)
  if sub is not None:
    sub.channelIds.remove(channelId)
    sub.save()
  return Response() 