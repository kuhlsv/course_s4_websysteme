"""
Provides the API for fetching uploaded files
"""
import io

from db.models.images import Image
from db.models.upload_file import UploadFile

from flask import Blueprint, Response, request, send_file
from json import dumps

def init(current_app):
  '''
  Generates a blueprint which handles uploads
  '''
  app = Blueprint('uploads', import_name='uploads', url_prefix='/uploads')

  @app.route('<id>', methods=['GET'])
  def get_entity(id):
    print('asdasdasdasd')
    # Fetch the file and send it to the user
    with open("uploads/" + id, 'rb') as bites:
      return send_file(
        io.BytesIO(bites.read()),
        attachment_filename=id,
        mimetype='image/png'
      )
  return app