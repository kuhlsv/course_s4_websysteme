from flask import Blueprint, Response, jsonify, redirect, request
from flask_jwt import jwt_required, current_identity

from db.models.user import User

def init(current_app):
  '''
  Creates a blueprint for the users api.
  '''
  app = Blueprint('users', import_name='users', url_prefix='/user')
    
  @app.route('/me', methods=['GET'])
  @jwt_required()
  def get_user():
    '''
    Fetches the current user if someone is logged in (and the JWT is passed in Header)
    '''
    user = User.objects.get(id=current_identity.id)
    user.password = None
    return Response(user.to_json())
  
  @app.route('/activate/<id>', methods=['GET'])
  def activate_user(id):
    '''
    Activates an user account. It fetches the user with the passed id and sets the isActive flag to true. Afterwards
    the user gets redirected to the root URL.
    '''
    user = User.objects.get(id=id)
    user.isActive = True
    user.save()
    return redirect(request.url_root)

  return app