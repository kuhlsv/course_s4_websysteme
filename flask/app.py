#!/usr/bin/env python3
from flask import Flask, Response

from api.posts import blueprint as posts
from api.channels import blueprint as channels

from db.MongoEngine import mongo

from utils.jwt import init as init_jwt
from utils.interceptors import register_interceptor
from utils.error_handling import register_error_handling

from api.images import init as init_image
from api.news import init as init_news
from api.uploads import init as init_uploads
from api.users.users import init as init_user
from api.subscriptions import app as subscriptions

from api.auth import app as auth

app = Flask(__name__)

# Using the config file definied in the FLASK_CONFIG environment variable
app.config.from_envvar('FLASK_CONFIG')

# Registration of our news, post, channel blueprints providing REST API
app.register_blueprint(channels)
app.register_blueprint(init_image(app))
app.register_blueprint(init_news(app))
app.register_blueprint(init_uploads(app))
app.register_blueprint(init_user(app))
app.register_blueprint(posts)
app.register_blueprint(auth)
app.register_blueprint(subscriptions)

# Registration of our custom interceptor. It sets the content type of each request to
# application/json by default
register_interceptor(app)

register_error_handling(app)

# Initialize pymongo. Make sure this will be called after initializing settings
mongo.init_app(app)

init_jwt(app)

@app.route("/")
def index():
    return Response(response="Welcome to flask version of FLACS", status=200)

@app.route("/admin")
def admin():
    return Response(response="Welcome to the Backend", status=200)
    # return redirect("http://localhost:8080", code=302)

if __name__ == "__main__":
  app.run()
