import datetime
import json
from collections import Iterable

import mongoengine_goodjson as gj
from bson import ObjectId
from bson.dbref import DBRef
from bson.objectid import ObjectId
from flask_mongoengine import MongoEngine
from mongoengine import (DateTimeField, IntField, ObjectIdField, StringField)

mongo = MongoEngine()