from db.MongoEngine import mongo

from bson import ObjectId

import mongoengine_goodjson as gj
from flask_mongoengine import MongoEngine
from mongoengine import (DateTimeField, IntField, ObjectIdField, StringField)

class Channel(gj.Document):
  '''
  Represents a channel
  '''
  # Make sure this is set to false, as it also verifies the existance when a user passes a new channel with no id
  #id = ObjectIdField(required=False, default=ObjectId())
  name = StringField(required=True)
  # Version field is taken from strapi. Just copied it for compability reasons
  v = IntField(db_field="__v")
  # Disabling strict mode, as it makes some problems sometimes
  meta = {'strict': False, 'collection': 'channels'}