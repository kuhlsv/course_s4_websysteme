from db.MongoEngine import mongo

import mongoengine_goodjson as gj
from flask_mongoengine import MongoEngine
from mongoengine import (DateTimeField, IntField, ObjectIdField, StringField, ReferenceField)
from db.models.upload_file import UploadFile

class Image(gj.Document):
  '''
  Represents an image
  '''
  id = ObjectIdField(required=False)
  name = StringField(required=True)
  v = IntField(db_field="__v")
  meta = {'strict': False, 'collection': 'images'}