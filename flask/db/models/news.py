from db.MongoEngine import mongo

import mongoengine_goodjson as gj
from flask_mongoengine import MongoEngine
from mongoengine import (DateTimeField, IntField, ObjectIdField, StringField)

class News(gj.Document):
  """
  News model using gj.Document as superclass, as the mongoengine Document uses strange serialization of ObjectIds,
  dates etc. Instead of serializing ObjectIds to an object like {$oid: ...} it will be serialized to a simple string.
  """
  title = StringField(required=True)
  location = StringField(required=True)
  text = StringField(required=False)
  image = StringField(required=False)
  updatedAt = DateTimeField(required=False)
  createdAt = DateTimeField(required=False)
  v = IntField(db_field="__v")
  meta = {'strict': False, 'collection': 'news'}