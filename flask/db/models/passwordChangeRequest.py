from db.MongoEngine import mongo

import mongoengine_goodjson as gj
from flask_mongoengine import MongoEngine
from mongoengine import (StringField)

class PasswordChangeRequest(gj.Document):
  '''
  Represents a password change request. This wont't be saved in mongodb but we use this anyway as mongoengine
  provides methods for validating this object.
  '''
  password = StringField(required=True)
  passwordConfirmation = StringField(required=True)
  oldPassword = StringField(required=True)
