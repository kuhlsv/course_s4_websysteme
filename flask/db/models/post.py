from db.MongoEngine import mongo

import mongoengine_goodjson as gj
from flask_mongoengine import MongoEngine
from mongoengine import (DateTimeField, IntField, ObjectIdField, StringField)

class Post(gj.Document):
  '''
  Represents a Post object
  '''
  id = ObjectIdField()
  channelId = StringField(required=True)
  user = StringField(required=True)
  content = StringField(required=True)
  v = IntField(db_field="__v")
  meta = {'collection': 'posts'}