from db.MongoEngine import mongo

import mongoengine_goodjson as gj
from flask_mongoengine import MongoEngine
from mongoengine import (StringField, EmailField)

class Registration(gj.Document):
  '''
  Represents a registration request. This wont't be saved in mongodb but we use this anyway as mongoengine
  provides methods for validating this object.
  '''
  username = StringField(required=True)
  password = StringField(required=True)
  email = EmailField(required=True)
