from db.MongoEngine import mongo

import mongoengine_goodjson as gj
from mongoengine import (StringField, ListField)

class Subscription(gj.Document):
  '''
  Representation of a subscription.
  '''
  userId = StringField(required=True)
  channelIds = ListField(StringField())
  meta = {'collection': 'subscriptions', 'strict': False}
