from db.MongoEngine import mongo

import mongoengine_goodjson as gj
from flask_mongoengine import MongoEngine
from mongoengine import (DateTimeField, IntField, ObjectIdField, StringField, ListField)

class UploadFile(gj.Document):
  id = ObjectIdField(required=False)
  url = StringField(required=True)
  related = ListField(required=False)
  v = IntField(db_field="__v")
  meta = {'strict': False, 'collection': 'upload_file'}