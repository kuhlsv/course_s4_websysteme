from db.MongoEngine import mongo

from bson import ObjectId
import mongoengine_goodjson as gj
from flask_mongoengine import MongoEngine
from mongoengine import (DateTimeField, IntField, ObjectIdField, StringField, BooleanField, ListField)

class AuthIdentity(object):

  '''
  Represents a logged in user. We could use the user class model directly, but ObjectIds seem to break
  the flask_jwt library, so we decided to use this workaround.
  '''
  def __init__(self, id=None, username=None, role=None):
    self.id = str(id) # Important. Do not change this to ObjectId!
    self.username = username
    self.role = role

class User(gj.Document):
  '''
  Represents a user
  '''
  id = ObjectIdField(primary_key=True, default=ObjectId(), required=True)
  email = StringField(unique=True, required=True)
  password = StringField(required=True)
  username = StringField(required=True, unique=True)
  #channels = StringField()
  #v = IntField(db_field="__v")
  role = StringField(default="User")
  isActive = BooleanField(default=False, required=True)
  meta = {'collection': 'flaskusers', 'strict': False}

  def to_auth_identity(self):
    print(self.to_mongo().to_dict())
    return AuthIdentity(id=self.id, username=self.username, role=self.role)