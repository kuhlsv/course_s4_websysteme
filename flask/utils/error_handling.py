from flask import Response
from mongoengine.errors import ValidationError

def register_error_handling(app):

  # Flask allows setting up a custom error handling whenever a certain Error occures. Here we're
  # going to register a handler for ValidationErrors which occure, whenever a user gives us a JSON
  # which is not compatible with our data model.
  @app.errorhandler(ValidationError)
  def handle_validation_error(e):
    return Response(response=e.message, status=400)