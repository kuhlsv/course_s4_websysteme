def register_interceptor(app):
  @app.after_request
  def append_json(response):
    """
    Provides our custom interceptor appending application/json as content type to all of our resonses
    """
    response.headers['Content-Type'] = 'application/json'
    return response
  
  @app.after_request
  def append_cors(response):
    """
    Adding CORS headers
    """
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    return response