import bcrypt
from flask import abort, jsonify
from flask_jwt import JWT, jwt_required

from db.models.user import User

def init(current_app):
  '''
  Adds everything which has to do with the flask_jwt library to our Flask instance
  '''

  def authenticate(username, password):
    '''
    Implements the authentication check for flask_jwt
    '''
    try:
      # First we're going to fetch the user with the given name. We'll take query only user
      # which do have the isActive flag set to true
      user = User.objects.get(username=username, isActive=True)

      # Checking the password hash
      if bcrypt.hashpw(password, user.password) == False:
        # If it does not equals flask_jwt awaits None
        return None
      
      # If everything worked, we're going to return the AuthIdentity which is going to be created
      # based on the given user object
      return User.objects.get(username=username).to_auth_identity()
    except:
      pass
    return None

  def identity(payload):
    '''
    Implementation of a method fetching the AuthIdentity based on the username.
    '''
    return User.objects.get(id=payload['identity']).to_auth_identity()

  def get_jwt_response(token=None, user=None):
    '''
    Implementation of the method returning the credentials after logging in
    '''
    return jsonify({ "jwt": token.decode('UTF-8'), "user": user.__dict__})

  jwt = JWT(current_app, authenticate, identity)
  jwt.auth_response_callback = get_jwt_response

  def set_header(identity):
    # Other interceptors do not work for the JWT part of our application so we pass the addidional headers here
    return {'Access-Control-Allow-Origin': '*', 
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
      'Access-Control-Allow-Methods': 'GET, POST, OPTIONS'}

  jwt.jwt_headers_callback = set_header

def check_role(current_identity, role):
  '''
  Checks if a user does have a certain role
  '''
  print(current_identity)
  if current_identity.role != role:
    abort(403)
