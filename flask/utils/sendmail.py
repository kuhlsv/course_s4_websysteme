

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText 

def send(to=None, subject=None, plaintext=None, html=None):
  '''
  Sends a mail by strapi@acrux.io to the passed receipient. It sneds both the HTML and the plain/text version
  of the mail
  '''
  fromaddr = "Gandalph Team <strapi@acrux.io>"
  msg = MIMEMultipart('alternative')
  msg['From'] = fromaddr
  msg['To'] = to
  msg['Subject'] = subject
  
  body = subject
  msg.attach(MIMEText(plaintext, 'plain'))
  msg.attach(MIMEText(html, 'html'))
  
  server = smtplib.SMTP('smtp.zoho.com', 587)
  server.starttls()
  server.login("strapi@acrux.io", "strapi007")
  text = msg.as_string()
  server.sendmail(fromaddr, to, text)
  server.quit()