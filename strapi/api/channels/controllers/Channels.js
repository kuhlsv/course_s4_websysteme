'use strict';

/**
 * Channels.js controller
 *
 * @description: A set of functions called "actions" for managing `Channels`.
 */

module.exports = {

  /**
   * Retrieve channels records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    return strapi.services.channels.fetchAll(ctx.query);
  },

  /**
   * Retrieve a channels record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.channels.fetch(ctx.params);
  },

  /**
   * Retrieve all posts of a channel.
   *
   * @return {Object}
   */

  posts: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.channels.posts(ctx.params);
  },

  /**
   * Couts the number of posts in a channel.
   *
   * @return {Object}
   */

  count: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.channels.count(ctx.params);
  },

  /**
   * Create a/an channels record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.channels.add(ctx.request.body);
  },

  /**
   * Update a/an channels record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.channels.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an channels record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.channels.remove(ctx.params);
  }
};
