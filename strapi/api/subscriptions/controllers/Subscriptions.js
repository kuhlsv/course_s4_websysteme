'use strict';

/**
 * Subscriptions.js controller
 *
 * @description: A set of functions called "actions" for managing `Subscriptions`.
 */

module.exports = {
  /**
   * Retrieve a subscriptions record.
   *
   * @return {Object}
   */

  find: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.subscriptions.find(ctx.params);
  },

  /**
   * Create a/an subscriptions record.
   *
   * @return {Object}
   */

  subscribe: async (ctx) => {
    return strapi.services.subscriptions.subscribe(ctx);
  },

  /**
   * Destroy a/an subscriptions record.
   *
   * @return {Object}
   */

  delete: async (ctx, next) => {
    return strapi.services.subscriptions.delete(ctx);
  }
};
