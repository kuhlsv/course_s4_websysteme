'use strict';

/**
 * Subscriptions.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const _ = require('lodash');

module.exports = {
  /**
   * Promise to fetch a/an subscriptions.
   *
   * @return {Promise}
   */

  find: (params) => {
    return Subscriptions
      .findOne({ userId: params._id });
  },

  /**
   * Promise to add a/an subscriptions.
   *
   * @return {Promise}
   */

  subscribe: async (ctx) => {
    return Subscriptions.findOneAndUpdate({
      userId: ctx.state.user._id
    }, {
      $addToSet: { channelIds: ctx.request.body.channelId }
    }, (a, b) => {});
  },

  delete: async (ctx) => {
    return Subscriptions.update({
      userId: ctx.state.user._id
    }, {
      $pull: {
        channelIds: ctx.params._id
      }
    }, (a, b) => {});
  }
};
