console.log("xxxxx");

module.exports = strapi => {
  return {

    defaults: {
      enabled: true
    },

    /**
     * Initialize the hook
     */

    initialize: cb => {
      var hostname = strapi.config.websocket.hostname;
      if (!hostname) {
        throw "Must provide hostname in config!";
      }

      console.log("Hello");

      cb();
      strapi.connections['websocket'] = new WebSocket(hostname);

    }
  };

};