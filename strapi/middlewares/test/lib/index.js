module.exports = strapi => {
  return {
    initialize: function(cb) {
      strapi.app.use(async (ctx, next) => {
        const start = Date.now();

        await next();

        const delta = Math.ceil(Date.now() - start);
        console.log("xxxxxx");

        // Set X-Response-Time header
        ctx.set('X-Response-Time', delta + 'ms');
      });

      cb();
    }
  };
};
