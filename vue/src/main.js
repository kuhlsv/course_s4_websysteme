import App from './app.vue';
import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';
import { createRouter } from './router';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// Admin
import AdminNav from './components/admin/nav';
// Forum
import ForumAllChannels from './components/forum/allChannels';
import ForumMessage from './components/forum/message';
import ForumNavbar from './components/forum/navbar';
import ForumPost from './components/forum/post';
import ForumSubscribedChannels from './components/forum/subscribedChannels';
import ForumUserSettings from './components/forum/userSettings';
// Front
import AcceptInvitation from './components/forum/acceptInvitationContent';
import Home from './components/home';
import HomeAboutUs from './components/home/aboutUs';
import HomeImprint from './components/home/imprint';
import HomeIntro from './components/home/intro';
import HomeNavbar from './components/home/navbar';
import HomeNews from './components/home/news';
import HomeShopFlensburg from './components/home/shop-flensburg';
import HomeShopKiel from './components/home/shop-kiel';
import HomeShopRendsburg from './components/home/shop-rendsburg';
import HomeShops from './components/home/shops';
import HomeContent from './components/home/siteContent';
import PrivateChatEmbedded from './components/chat/privateChat';
// Helper
import AsyncImage from './components/asyncImage';
import MessageCount from './components/chat/messageCount.vue';

// Create router
const router = createRouter();

// Load components
Vue.use(BootstrapVue);
Vue.component('admin-nav', AdminNav);
Vue.component('forum-all-channels', ForumAllChannels);
Vue.component('forum-message', ForumMessage);
Vue.component('forum-navbar', ForumNavbar);
Vue.component('forum-post', ForumPost);
Vue.component('forum-subscribed-channels', ForumSubscribedChannels);
Vue.component('forum-user-settings', ForumUserSettings);
Vue.component('forum-invitation', AcceptInvitation);
Vue.component('home-about-us', HomeAboutUs);
Vue.component('home-content', HomeContent);
Vue.component('home-imprint', HomeImprint);
Vue.component('home-intro', HomeIntro);
Vue.component('home-navbar', HomeNavbar);
Vue.component('home-news', HomeNews);
Vue.component('home-shop-flensburg', HomeShopFlensburg);
Vue.component('home-shop-kiel', HomeShopKiel);
Vue.component('home-shop-rendsburg', HomeShopRendsburg);
Vue.component('home-shops', HomeShops);
Vue.component('home', Home);
Vue.component('private-chat', PrivateChatEmbedded);
Vue.component('message-count', MessageCount);
Vue.component('async-image', AsyncImage)

// Config
global.config = {
  location: [ 'Flensburg', 'Kiel', 'Rendsburg' ],
  userRoles: [ 'Administrator', 'Authenticated', 'Public' ]
}

// creates the Vue instance
const app = new Vue({
  el: '#app',
  router,
  config,
  render: h => h(App)
})