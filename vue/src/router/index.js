import Vue from 'vue';
import Router from 'vue-router';
import authService from '../services/AuthService';

Vue.use(Router);

// Admin
const Dash = () => import('../components/admin/dash.vue')
const News = () => import('../components/admin/news.vue')
const Channels = () => import('../components/admin/channels.vue')
const Posts = () => import('../components/admin/posts.vue')
const Upload = () => import('../components/admin/upload.vue')
const Users = () => import('../components/admin/users.vue')
const NewNews = () => import('../components/admin/new-news.vue')
const NewChannels = () => import('../components/admin/new-channels.vue')
const NewPosts = () => import('../components/admin/new-posts.vue')
const NewUsers = () => import('../components/admin/new-users.vue')
// Front & Forum
const Home = () => import('../components/home.vue')
const Forum = () => import('../components/forum.vue')
const Channel = () => import('../components/channel.vue')
const ChannelOverview = () => import('../components/channelOverview.vue')
const UserSettings = () => import('../components/userSettings.vue')
const Login = () => import('../components/globalLogin.vue')
const Chat = () => import('../components/chat.vue')
const PrivateChat = () => import('../components/chat/privateChat.vue')
const ChatList = () => import('../components/chatlist.vue')
const Registration = () => import('../components/registration.vue')
const AcceptInvitation = () => import('../components/acceptInvitation.vue')

export function createRouter () {
  let router = new Router({
    fallback: false,
    // mode: 'history', // Hash(none-request) / History(request) routing
    mode: 'hash',
    //fallback: false,
    //scrollBehavior: () => ({ y: 0 }),
    routes: [
      // Front
      { path: '/', component: Home, name: 'home' },
      // Admin
      { path: '/admin/', name: 'aDash', component: Dash }, /* With hash-routing: /#/admin */
      { path: '/admin/user', component: UserSettings },
      { path: '/admin/login', component: Login },
      { path: '/admin/news', name: 'aNews', component: News },
      { path: '/admin/channels', name: 'aChannels', component: Channels },
      { path: '/admin/posts', name: 'aPosts', component: Posts },
      { path: '/admin/upload', name: 'aUpload', component: Upload },
      { path: '/admin/users', name: 'aUsers', component: Users },
      { path: '/admin/news/create', name: 'create-aNews', props: true, component: NewNews },
      { path: '/admin/channels/create', name: 'create-aChannels', props: true, component: NewChannels },
      { path: '/admin/posts/create', name: 'create-aPosts', props: true, props: true, component: NewPosts },
      { path: '/admin/users/create', name: 'create-aUsers', props: true, component: NewUsers },
      //Forum
      { path: '/forum', component: Forum },
      { path: '/forum/channels', component: ChannelOverview },
      { path: '/forum/channel/:id', name: 'channel', component: Channel },
      { path: '/forum/user', component: UserSettings },
      { path: '/forum/login', component: Login },
      { path: '/chats', name: 'chatlist', component: ChatList },
      { path: '/chat/:user', name: 'chat.private', component: Chat },
      { path: '/login', name: 'login', component: Login },
      { path: '/forum/register', component: Registration },
      { path: '/forum/channel/:id/invite', component: AcceptInvitation }
    ]
  });
  authService.setRouter(router);
  return router;
};
