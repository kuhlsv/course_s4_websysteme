import Vue from 'vue';
import axios from 'axios';

/**
 * Authentication service provides methods for authenticating users against the /auth/local endpoint. It POSTs 
 * the credentials to the endpoint and receives the user data and a JWT token. The token will be appended to all
 * axios requests as Bearer token.
 * 
 * @author cfender
 */
class AuthService {

  constructor() {
    console.log("AuthService");
    this._user = null;
    this._token = null;
    this._router = null;
    this.setAxiosHeader();
  }

  /**
   * Sets up an interceptor, whenever an 401 is returned the user will be directed to the login form.
   * 
   * @param {vue-router} router 
   */
  setRouter(router) {
    this._router = router;
    axios.interceptors.response.use(function (response) {
      return response;
    }, function (error) {
      if (error.response && error.response.status === 401) {
        router.push({ path: '/login',
          query: {
            redirectTo: router.fullPath
          }
        });
      }
      return Promise.reject(error);
    });
  }

  getCurrentUser() {
    if (this._user) {
      return new Promise((resolve, reject) => resolve(this._user));
    } else {
      return axios.get(`${process.env.ROOT_API}/user/me`)
        .then((response) => response.data)
        .catch((error) => {
          console.log(`[AuthService] Could not get current user! ${error}`)
          if (error.response && error.response.status === 400) {
            this._router.push({ path: '/login',
              query: {
                redirectTo: this._router.fullPath
              }
            });
          }
        });
    }
  }

  /**
   * Authenticates a user.
   * 
   * @param {string} username 
   * @param {string} password 
   * @return {Promise}
   */
  login(username, password) {
    localStorage.removeItem('token');
    return axios.post(`${process.env.ROOT_API}/auth/local`, {
      identifier: username,
      password
    })
    .then(response => response.data)
    .then(response => {
      this._user = response.user;
      this._token = response.jwt;
      localStorage.setItem('token', response.jwt);
      this.setAxiosHeader();
      return response;
    });
    ;
  }

  /**
   * Sets the authentication head for axios
   */
  setAxiosHeader() {
    console.log("Setup interceptor");
    // Add a request interceptor for adding JWT token to all further requests
    axios.interceptors.request.use(function (config) {
      let token = localStorage.getItem('token');
      console.log(token);
      if (token) {
        config.headers['Authorization'] = 'Bearer ' + token;
      }
      return config;
    });
  }

  /**
   * Returns the token
   * @returns {string}
   */
  getToken() {
    return localStorage.getItem('token');
  }

  /**
   * Checks if the user is already authenticated
   * @returns {boolean}
   */
  isAuthenticated() {
    return this.getToken() !== null;
  }

  logout() {
    this._token = null;
    this._user = null;
    localStorage.removeItem('token');
    this._router.push({ path: '/', query: {
      loggedOut: true
    }});
  }

  register(user) {
    return axios.post(`${process.env.ROOT_API}/auth/local/register`, user)
      .then(response => response.data);
  }

}

export default new AuthService();