import * as axios from 'axios';

// uploads a file to the server
function upload(formData) {
    const url = `/uploads`;
    return axios.post(url, formData)
        // get data
        .then(x => x.data)
        // add url field
        .then(x => x.map(img => Object.assign({}, img, { url: `/images/${img.id}` })));
}

export { upload }
