import authService from '../services/AuthService';

var webSocketService = null;

/**
 * Handles subscribing and unsubscribing from websocket topics
 */
class WebSocketService {
  
  constructor() {
    console.log("Construct websocket service");
    this._ws = new WebSocket("ws://" + window.location.hostname + "/ws"); //new WebSocket(`${process.env.WEBSOCKET_API}`);
    this._ws.onmessage = this._onMessage.bind(this);
    this._listeners = {};
  }

  _onMessage(message) {
    let msg = JSON.parse(message.data);
    console.info('[WebsocketService] Got message!', msg);
    switch (msg.cmd) {
      case 'msg':
        if (this._listeners[msg.payload.type]) {
          this._listeners[msg.payload.type].forEach((listener) => {
            listener(msg.payload);
          });
        }
        break;
      case 'ping':
        this._ws.send(JSON.stringify({
          cmd: 'pong'
        }));
        break;
    }
  }

  /**
   * Subscribes to the given topic
   * @param {string} type 
   * @param {function} callback function that is called when a new messag arrives
   */
  subscribe(type, callback) {
    if (!this._listeners[type]) {
      this._listeners[type] = [];
    }
    let index = this._listeners[type].push(callback);
    console.info('[WebsocketService] Subscribed type', type, callback);
    // Return closure for unsubscribing
    return () => {
      console.info('[WebsocketService] Destroy!');
      this._listeners[type].shift(this._listeners[type], 1);
    }
  }

  // gets the instance of this class
  static getInstance() {
    console.info('[WebsocketService] Instance requested');
    if (webSocketService == null) {
      webSocketService = new WebSocketService();
    }
    return webSocketService;
  }

  /**
   * Updates the entities in the passed array, based on the incoming websocket messages.
   * 
   * @param {string} type 
   * @param {Array} entities 
   * @return {function} A function for unsubscribing
   */
  static handleType(type, controller, property) {
    console.log("Handle type", type, WebSocketService.getInstance());
    return WebSocketService.getInstance().subscribe(type, (payload) => {
      switch (payload.action) {
        case 'insert':
          controller[property].unshift(payload.doc);
          break;
        case 'delete':
          controller[property] = controller[property].filter(entity => entity.id !== payload.id);
          break;
        case 'replace':
          controller[property] = controller[property].map((entity) => {
            if (entity.id !== payload.id) return entity;
            return payload.doc;
          });
          break;
        case 'update':
          controller[property] = controller[property].map((entity) => {
            if (entity.id !== payload.id) return entity;
            return Object.assign({}, entity, payload.update);
          });
          break;
        default:
          break;
        }
    });
  }

  static safeUnsubscribe(unsubscribe) {
    if (unsubscribe) {
      try {
        unsubscribe();
      } catch (e) {
        // Nothing to do here
      }
    }
  }

}

export default WebSocketService;