#!/usr/bin/node
const debug = require('debug')('main'),
  WebSocketServer = require('uws').Server,
  settings = require('./settings.json'),
  wss = new WebSocketServer({ port: process.env.WS_PORT || settings.server.port, host: settings.server.host }),
  handleChange = require('./streamHandler.js')(wss),
  MongoListener = require('./mongoListener.js'),
  keepalive = require('./keepalive.js')(wss);

new MongoListener(settings.mongodb).init(handleChange);