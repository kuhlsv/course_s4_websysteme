const settings = require('./settings.json'),
  MongoClient = require('mongodb').MongoClient,
  debug = require('debug')('mongoListener');

class MongoListener {

  constructor(settings) {
    this._settings = settings;
  }

  init(changeHandler) {
    debug('Initializing connection');
    MongoClient.connect(this._settings.uri, {
      useNewUrlParser: true
    }).then((client) => {
      debug('Etablished MongoDB connection!');
      const db = client.db(this._settings.database);
      this._settings.collections
        .map((collection) => db.collection(collection))
        .map((collection) => collection.watch({}))
        .map((stream) => stream.on('change', (change) => {
          changeHandler(change);
        }));
    });
  }

}

module.exports = MongoListener;