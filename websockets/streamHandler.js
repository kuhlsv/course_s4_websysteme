const debug = require('debug')('streamHandler');

module.exports = (ws) => {
  debug('Registered streamHandler callback');
  return (change) => {
    let payload = {
      action: change.operationType,
      type: change.ns.coll,
      id: change.documentKey._id
    };
    switch (change.operationType) {
      case 'insert':
        payload['doc'] = change.fullDocument;
        break;
      case 'update':
        payload['update'] = change.updateDescription.updatedFields;
        break;
      default:
        break;
    }
    if (payload.doc) {
      payload.doc.id = payload.doc._id;
    }
    let msg = JSON.stringify({
      payload,
      cmd: 'msg'
    });
    debug(`Broadcasting message: ${msg}`);
    ws.broadcast(msg);
  }
}